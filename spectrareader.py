from astropy.io import fits
import os
import pylab
from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
from astropy.modeling import models, fitting

# Reading an ALFALFA spectrum
# January 2019

def makeplot(fitsfile):

    print(fitsfile)

    # Open the fits file
    hdul = fits.open(fitsfile)

    # Examine the primary header
    primaryhdr = hdul['PRIMARY']

    # Get the binary table
    binarytable = hdul['Single Dish']

    # Get the four-column binary table data
    spectrum = binarytable.data

    # Display the columns
    # print(spectrum.columns)
    
    # Fit the spectrum to determine the velocity
    g_init = models.Gaussian1D(amplitude=60, mean=3500, stddev=300.)
    fit_g = fitting.LevMarLSQFitter()
    g = fit_g(g_init, spectrum.field('VHELIO'), spectrum.field('FLUX'))

    # Make a plot of the spectrum
    fig = plt.figure(figsize=(6,4), facecolor='white')
    ax = fig.add_subplot(111)

    ax.plot(spectrum.field('VHELIO'), spectrum.field('FLUX'), color='blue', label='Galaxy Spectrum')
    ax.axhline(y=0.0, linewidth=1, color='grey', linestyle='--')
    plt.plot(spectrum.field('VHELIO'), g(spectrum.field('VHELIO')), color='red', label='Line Fit')
    plt.legend(loc=0)
    
    # Print fit parameters
    print(g)

    ax.set_xlabel('Velocity cz_sun (km/s)')
    ax.set_ylabel('Flux (mJy)')
    ax.set_title(fitsfile)

    plt.savefig(os.path.basename(fitsfile) + '.png', dpi=300)

    #plt.show()

    hdul.close()
    plt.close('all')

# Name of a test fits file
fitsfile = 'A006950.fits'

try:
    makeplot(fitsfile)
except Exception as e:
    print("ERROR " + str(e) + " with " + fitsfile)